import express from 'express';

import authRequest from '../../middlewares/authRequest';
import UsersController from '../../controllers/UsersController';

const router = express.Router();


router.route('/current')
    .get(authRequest, UsersController.getCurrentUser);


export default router;
