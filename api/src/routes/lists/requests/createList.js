import {check} from 'express-validator/check';

import requestValidator from '../../../middlewares/requestValidator';

const validators = [
    check('name')
        .exists()
        .withMessage('Введите имя списка')

        .isLength({ min: 1 })
        .withMessage('Введите имя списка'),

    requestValidator
];

export default validators;
