import express from 'express';

import authRequest from '../../middlewares/authRequest';
import requests from './requests';
import ListsController from '../../controllers/ListsController';

const router = express.Router();

router.route('/')
    .get(authRequest, ListsController.getAllByUserId)
    .post(authRequest, requests.createList, ListsController.create);

export default router;
