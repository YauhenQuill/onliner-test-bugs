import createTask from './createTask';
import completeTask from './completeTask';
import deleteTask from './deleteTask';

export default {
    createTask,
    completeTask,
    deleteTask,
};
