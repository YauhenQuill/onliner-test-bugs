import {ForbiddenException} from '../../../exceptions';

import {check} from 'express-validator/check';

import {taskIsExist, userIsTaskOwner} from '../../../services/validators'
import requestValidator from '../../../middlewares/requestValidator';

const validators = [
    check('id')
        .custom(taskIsExist)
        .withMessage(() => new ForbiddenException('Такой задачи нет').getWrapped())

        .custom(userIsTaskOwner)
        .withMessage(() => new ForbiddenException('Вы не можете этого сделать').getWrapped()),

    requestValidator
];

export default validators;
