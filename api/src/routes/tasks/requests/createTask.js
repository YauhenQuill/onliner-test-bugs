import {check} from 'express-validator/check';

import {listIsExist} from '../../../services/validators'
import requestValidator from '../../../middlewares/requestValidator';

const validators = [
    check('name')
        .exists()
        .withMessage('Введите имя задачи')

        .isLength({ min: 1 })
        .withMessage('Введите имя задачи'),

    check('description')
        .isLength({ max: 320 })
        .withMessage('Максимальная длинна описания - 550 символов'),

    check('list_id')
        .optional()
        .custom(listIsExist)
        .withMessage('Список не существует'),

    requestValidator
];

export default validators;
