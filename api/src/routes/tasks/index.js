import express from 'express';

import authRequest from '../../middlewares/authRequest';
import requests from './requests';
import TasksController from '../../controllers/TasksController';

const router = express.Router();

router.route('/')
    .get(authRequest,  TasksController.getAll)
    .post(authRequest, requests.createTask, TasksController.create);

router.route('/:id/complete')
    .post(authRequest, requests.completeTask, TasksController.completeTask);

router.route('/:id/delete')
    .delete(authRequest, requests.deleteTask, TasksController.delete);

export default router;
