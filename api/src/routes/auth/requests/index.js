import registration from './registration';
import login from './login';

export default {
    registration,
    login
};
