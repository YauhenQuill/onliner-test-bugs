import {check} from 'express-validator/check';
import {ForbiddenException} from '../../../exceptions';

import {userPasswordValidator} from '../../../services/validators'
import setUserByBodyEmail from '../../../middlewares/validatorsHelpers/setUserByBodyEmail';
import requestValidator from '../../../middlewares/requestValidator';

const validators = [
    setUserByBodyEmail,

    check('email')
        .exists()
        .withMessage('Введите email')

        .isLength({ min: 1 })
        .withMessage('Введите email')

        .isEmail()
        .withMessage('Неверный email'),

    check('password')
        .exists()
        .withMessage('Введите пароль')

        .isLength({ min: 1 })
        .withMessage('Введите пароль')

        .isLength({ min: 6, max: 16 })
        .withMessage('Неверная длина пароля'),

    check()
        .custom(userPasswordValidator)
        .withMessage(() => new ForbiddenException('Неверный email или пароль').getWrapped()),

    requestValidator,
];

export default validators;
