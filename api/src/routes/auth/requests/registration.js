import {check} from 'express-validator/check';

import {userIsNotRegistered} from '../../../services/validators'
import setUserByBodyEmail from '../../../middlewares/validatorsHelpers/setUserByBodyEmail';
import requestValidator from '../../../middlewares/requestValidator';

const validators = [
    setUserByBodyEmail,

    check('email')
        .exists()
        .withMessage('Введите email')

        .isLength({ min: 1 })
        .withMessage('Введите email')

        .isEmail()
        .withMessage('Неверный email')

        .custom(userIsNotRegistered)
        .withMessage('Этот email уже использется'),

    check('password')
        .exists()
        .withMessage('Введите пароль')

        .isLength({ min: 1 })
        .withMessage('Введите пароль')

        .isLength({ min: 6, max: 16 })
        .withMessage('Неверная длина пароля'),

    check('name')
        .exists()
        .withMessage('Введите имя')

        .isLength({ min: 1 })
        .withMessage('Введите имя'),

    requestValidator
];

export default validators;
