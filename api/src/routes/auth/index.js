import express from 'express';

import requests from './requests';
import AuthController from '../../controllers/AuthController';

const router = express.Router();

router.route('/login')
    .post(requests.login, AuthController.login);

router.route('/registration')
    .post(requests.registration, AuthController.registration);

export default router;
