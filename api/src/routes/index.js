import express from 'express';
import cors from 'cors';

import authRouters from './auth';
import listsRouters from './lists';
import tasksRouters from './tasks';
import usersRouters from './users';

const router = express.Router();

router.use(cors());

router.use((req, res, next) => {
    req.validation = {};
    next();
});

router.use('/auth', authRouters);
router.use('/lists', listsRouters);
router.use('/tasks', tasksRouters);
router.use('/users', usersRouters);

router.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

router.use((err, req, res, next) => {
    if (!err.status) {
        console.log(err);

        res.status(500);
        res.json();

        return;
    }

    res.status(err.status );
    err.errorMessage = err.message;
    res.json(err);
});

export default router;
