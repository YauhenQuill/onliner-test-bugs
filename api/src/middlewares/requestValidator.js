import _ from 'underscore';
import {ValidationException} from '../exceptions';

const middleware = (req, res, next) => {
    req.asyncValidationErrors(true)
        .then(() => next())
        .catch((errors) => {
            return next(getFirstException(errors) || new ValidationException(mapErrors(errors)));
        });
};

const getFirstException = (errors = {}) => {
    const exceptions = [];

    _.each(errors, error => error.msg.exception && exceptions.push(error.msg.exception));

    return exceptions[0];
};

const mapErrors = (errors = {}) => {
    return _.mapObject(errors, error => error.msg);
};

export default middleware;
