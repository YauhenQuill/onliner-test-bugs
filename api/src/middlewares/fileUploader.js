import multer from 'multer';
import moment from 'moment';
import fs from 'fs';

const tmpFolder = 'uploads-tmp';

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const currentDate = moment().format('DD-MM-YYYY');
        const destinationPath = `${tmpFolder}/${currentDate}/`;

        !fs.existsSync(destinationPath) && fs.mkdirSync(destinationPath);

        cb(null, destinationPath); //todo: обработать ошибку, когда нет файла
    }
});

const upload = multer({ storage: storage });

export default upload.single('file');
