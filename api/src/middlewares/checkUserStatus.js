import ForbiddenException from '../exceptions/ForbiddenException';

const middleware = (statusKey = '', statuses = [], isContains = true, exceptionMessageKey = 'exception.forbidden') => {
    return (req, res, next) => {
        const user = req.user || {};
        const status = user[statusKey];

        if (!statusKey || !statuses.length || !status) {
            return next();
        }

        const userStatus = user[statusKey];
        const userStatusIsContains = statuses.some(status => status === userStatus);

        if (userStatusIsContains === isContains) {
            return next();
        }

        next(new ForbiddenException(global.__(exceptionMessageKey)));
    }
};

export default middleware;
