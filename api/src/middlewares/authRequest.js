import {User} from '../models';
import tokenService from '../services/tokenService';
import {UnauthorizedException} from '../exceptions';

const middleware = async(req, res, next) => {
    const token = req.headers.authorization;

    if (!token) {
        return next(new UnauthorizedException());
    }

    const verifyResult = tokenService.verify(token);

    if (!verifyResult.isValid || verifyResult.isExpired) {
        return next(new UnauthorizedException('Bad credentials'));
    }

    const user = await User.findById(verifyResult.data.id);

    if (!user) {
        return next(new UnauthorizedException('Bad credentials'));
    }

    req.user = user;

    next();
};

export default middleware;
