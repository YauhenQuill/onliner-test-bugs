import {User} from '../../models';

const middleware = async (req, res, next) => {
    const email = req.body.email;

    try {
        req.validation.user = await User.findByEmail(email);

        next();
    } catch (e) {
        next(e);
    }
};

export default middleware;
