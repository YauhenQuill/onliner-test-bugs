import User from '../../models/UserModel';

const middleware = async (req, res, next) => {
    try {
        const userId = req.params.userId;

        req.validation.user = await User.findById(userId);
    } catch (e) {
    }

    next();
};

export default middleware;
