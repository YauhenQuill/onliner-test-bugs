import User from '../../models/UserModel';
import tokenService from '../../services/tokenService';

const middleware = async (req, res, next) => {
    const accessToken = req.body.accessToken;
    const accessTokenData = tokenService.verify(accessToken).data || {};
    const userId = accessTokenData.id;

    req.validation.user = await User.findById(userId);

    next();
};

export default middleware;
