import BaseException from './BaseException';

class ValidationException extends BaseException{
    constructor(errors, message = '') {
        super(message || 'Failed validation', 422);

        this.errors = errors;
    }
}

export default ValidationException;
