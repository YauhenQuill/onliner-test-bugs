class BaseException extends Error{
    constructor (message, status) {
        super(message || 'Server error');

        this.status = status || 500;
    }

    getWrapped () {
        return { exception: this };
    }
}

export default BaseException;
