import BaseException from './BaseException';

class NotFoundException extends BaseException{
    constructor(message) {
        super(message || 'Not found', 404);
    }
}

export default NotFoundException;

