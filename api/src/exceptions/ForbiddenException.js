import BaseException from './BaseException';

class ForbiddenException extends BaseException{
    constructor(message) {
        super(message || global.__('exception.forbidden'), 403);
    }
}

export default ForbiddenException;
