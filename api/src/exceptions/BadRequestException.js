import BaseException from './BaseException';

class BadRequestException extends BaseException{
    constructor(message) {
        super(message || 'Bad Request', 400);
    }
}

export default BadRequestException;

