import BaseException from './BaseException';

class UnauthorizedException extends BaseException{
    constructor(message) {
        super(message || 'Unauthorized', 401);
    }
}

export default UnauthorizedException;

