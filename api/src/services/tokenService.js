import jwt from 'jsonwebtoken';

const accessTokenLifeTime = 60 * 10;

const generate = (data = {}) => {

    const accessToken = jwt.sign({data}, process.env.JWT_SECRET, { expiresIn: accessTokenLifeTime });

    return {
        accessToken,
        accessTokenLifeTime,
    }
};

const verify = (token) => {
    const result = {
        isValid: false,
        isExpired: false,
        data: {}
    };

    try {
        const decoded = jwt.verify(token, process.env.JWT_SECRET);

        result.isValid = true;
        result.isExpired = false;
        result.data = decoded.data;
    } catch (e) {
        const decoded = jwt.decode(token);

        result.isValid = e.name !== 'JsonWebTokenError';
        result.isExpired = e.name === 'TokenExpiredError';
        result.data = decoded ? decoded.data : undefined;
    }

    return result;
};

export default {
    generate,
    verify
}
