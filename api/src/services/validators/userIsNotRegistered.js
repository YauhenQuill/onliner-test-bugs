const validator = async (value, { req }) => {
    try {
        await req.asyncValidationErrors(true);
        const user = req.validation.user;

        return !user;
    } catch (e) {
        return true;
    }
};

export default validator;
