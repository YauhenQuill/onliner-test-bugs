import {List} from '../../models';

const validator = async (value, { req }) => {
    try {
        if (!value) {
            return true;
        }

        await req.asyncValidationErrors(true);

        return !!await List.findById(value);
    } catch (e) {
        return true;
    }
};

export default validator;
