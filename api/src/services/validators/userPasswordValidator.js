const validator = async(value, { req }) => {
    const password = req.body.password;

    try {
        await req.asyncValidationErrors(true);
        const user = req.validation.user;

        if (!user) {
            return false;
        }

        return await user.checkPassword(password);
    } catch (e) {
        return true;
    }
};

export default validator;
