import {Task} from '../../models';

const validator = async (value, { req }) => {
    try {
        await req.asyncValidationErrors(true);

        const task = await Task.findById(value);

        return task.user_id === req.user.id;
    } catch (e) {
        return true;
    }
};

export default validator;
