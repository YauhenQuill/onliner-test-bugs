const reg = /^[a-z0-9!@#$%^&*]+$/i;

const validator = async (value) => reg.test(value);

export default validator;
