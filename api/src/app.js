import http from 'http';
import express from 'express';
import logger from 'morgan';
import bodyParser from 'body-parser';
import expressValidator from 'express-validator';

require('dotenv').config();

import routes from './routes';

const app = express();
const server = http.createServer(app);

const startServer = () => {
    const port = process.env.PORT;

    server.listen(port);
    console.log(`App started. Port ${port}`);
};

startServer();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(expressValidator());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/api', routes);

export {
    app,
};
