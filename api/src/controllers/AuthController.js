import { matchedData } from 'express-validator/filter';
import {User} from '../models';

class AuthController {
    static async login(req, res, next) {
        try {
            const user = req.validation.user;
            const token = await user.getAuthToken();

            res.json(token);
        } catch (e) {
            return next(e);
        }
    }

    static async registration(req, res, next) {
        const userData = matchedData(req);

        try {
            const user = new User(userData);

            user.id = await user.create();
            const token = await user.getAuthToken();

            res.json(token);
        } catch (e) {
            next(e);
        }
    }
}

export default AuthController;
