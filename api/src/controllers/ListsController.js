import { matchedData } from 'express-validator/filter';
import { List } from '../models';

class ListsController {
    static async getAllByUserId(req, res, next) {
        try {
            const lists = await List.getAllByUserId(req.user.id);

            res.json(lists);
        } catch (e) {
            next(e);
        }
    }

    static async getById(req, res, next) {

    }

    static async create(req, res, next) {
        const data = matchedData(req);

        try {
            const list = new List(data);

            list.user_id = req.user.id;
            list.id = await list.create();

            res.json(list);
        } catch (e) {
            next(e);
        }
    }

    static async update(req, res, next) {

    }

    static async delete(req, res, next) {

    }
}

export default ListsController;
