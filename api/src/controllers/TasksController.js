import _ from 'underscore';

import { matchedData } from 'express-validator/filter';
import { Task, List } from '../models';

class ListsController {
    static async getAll(req, res, next) {
        try {
            const lists = await List.getAll();
            const tasks = await Task.getAll();

            lists.forEach((item) => {
                item.tasks = _.where(tasks, {list_id: item.id});
            });

            res.json({
                lists_tasks: lists,
                tasks_without_list: _.where(tasks, {list_id: null})
            });
        } catch (e) {
            next(e);
        }
    }

    static async getById(req, res, next) {

    }

    static async create(req, res, next) {
        const data = matchedData(req);

        try {
            const task = new Task(data);

            task.user_id = req.user.id;
            task.id = await task.create();

            res.json(task);
        } catch (e) {
            next(e);
        }
    }

    static async completeTask(req, res, next) {
        const task = await Task.findById(req.params.id);

        res.json(await task.complete());
    }

    static async delete(req, res, next) {
        const task = await Task.findById(req.params.id);

        await task.delete();
        res.status(200).send();
    }
}

export default ListsController;
