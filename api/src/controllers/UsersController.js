class UsersController {
    static getCurrentUser(req, res, next) {
        res.json(req.user);
    }
}

export default UsersController;
