import db from '../services/dbConnection'

class Task {
    constructor(data = {}) {
        this.id = data.id || null;
        this.name = data.name || '';
        this.description = data.description || '';
        this.is_complete = data.is_complete || 0;
        this.user_id = data.user_id || null;
        this.list_id = data.list_id || null;
    }

    create() {
        return new Promise((resolve, reject) => {
            const stmt = db.prepare("INSERT INTO tasks(name, description, user_id, list_id) VALUES (?, ?, ?, ?)", [
                this.name,
                this.description,
                this.user_id,
                this.list_id
            ]);

            stmt.run(function (err) {
                if (err) {
                    return reject(err);
                }

                resolve(this.lastID);
            });
        });
    }

    update() {}

    complete() {
        return new Promise((resolve, reject) => {
            db.run(`UPDATE tasks SET is_complete = 1 WHERE id = \'${this.id}\'`, (err, data) => {
                if (err) {
                    return reject(err);
                }

                resolve(data ? new Task(data) : undefined);
            })
        });
    }

    delete() {
        return new Promise((resolve, reject) => {
            db.run(`DELETE FROM tasks WHERE id = \'${this.id}\'`, (err) => {
                if (err) {
                    return reject(err);
                }

                resolve();
            })
        });
    }

    static getAll() {
        return new Promise((resolve, reject) => {
            db.all('SELECT * FROM tasks', (err, data) => {
                if (err) {
                    return reject(err);
                }

                resolve(data);
            });
        });
    }

    static getAllByListId(id) {
        return new Promise((resolve, reject) => {
            db.all(`SELECT * FROM tasks WHERE list_id = \'${id}\'`, (err, data) => {
                if (err) {
                    return reject(err);
                }

                resolve(data);
            });
        });
    }

    static getAllByListIds(ids = []) {
        return new Promise((resolve, reject) => {
            db.all(`SELECT * FROM tasks WHERE id IN (${ids.join(',')})`, (err, data) => {
                if (err) {
                    return reject(err);
                }

                resolve(data);
            });
        });
    }

    static findById(id) {
        return new Promise((resolve, reject) => {
            db.get(`SELECT * FROM tasks WHERE id = \'${id}\'`, (err, data) => {
                if (err) {
                    return reject(err);
                }

                resolve(data ? new Task(data) : undefined);
            })
        });
    }
}

export default Task;
