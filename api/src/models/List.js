import db from '../services/dbConnection'

class List {
    constructor(data = {}) {
        this.id = data.id || null;
        this.name = data.name || '';
        this.user_id = data.user_id || null;
    }

    create() {
        return new Promise((resolve, reject) => {
            const stmt = db.prepare("INSERT INTO lists(name, user_id) VALUES (?, ?)", [
                this.name,
                this.user_id
            ]);

            stmt.run(function (err) {
                if (err) {
                    return reject(err);
                }

                resolve(this.lastID);
            });
        });
    }

    update() {}

    static getAll() {
        return new Promise((resolve, reject) => {
            db.all('SELECT * FROM lists', (err, data) => {
                if (err) {
                    return reject(err);
                }

                resolve(data);
            });
        });
    }

    static getAllByUserId(id) {
        return new Promise((resolve, reject) => {
            db.all(`SELECT * FROM lists WHERE user_id = \'${id}\'`, (err, data) => {
                if (err) {
                    return reject(err);
                }

                resolve(data);
            });
        });
    }

    static findById(id) {
        return new Promise((resolve, reject) => {
            db.get(`SELECT * FROM lists WHERE id = \'${id}\'`, (err, data) => {
                if (err) {
                    return reject(err);
                }

                resolve(data ? new List(data) : undefined);
            })
        });
    }
}

export default List;
