import db from '../services/dbConnection'
import tokenService from '../services/tokenService';

class User {
    constructor(data = {}) {
        this.id = data.id || null;
        this.email = data.email || '';
        this.password = data.password || '';
        this.name = data.name || '';
    }

    async create() {
        return new Promise((resolve, reject) => {
            const stmt = db.prepare("INSERT INTO users(email, password, name) VALUES (?, ?, ?)", [
                this.email,
                this.password,
                this.name
            ]);

            stmt.run(function (err) {
                if (err) {
                    return reject(err);
                }

                resolve(this.lastID);
            });
        });
    }

    update() {}

    checkPassword(password) {
        return this.password === password;
    }

    getAuthToken() {
        return tokenService.generate({id: this.id});
    }

    static findById(id) {
        return new Promise((resolve, reject) => {
            db.get(`SELECT * FROM users WHERE id = \'${id}\'`, (err, data) => {
                if (err) {
                    return reject(err);
                }

                resolve(new User(data));
            })
        });
    }

    static findByEmail(email) {
        return new Promise((resolve, reject) => {
            db.get(`SELECT * FROM users WHERE email = \'${email}\'`, (err, data) => {
                if (err) {
                    return reject(err);
                }

                resolve(data ? new User(data) : undefined);
            })
        });
    }
}

export default User;
