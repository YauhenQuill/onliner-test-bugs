# SoilCRM API
## Общая информация
## Доступны следующие роуты:

| Роут | Описание |
|---|---|
| POST [/auth/login ](/docs/auth/login.md)| Аутификация |
| POST [/auth/refresh-tokens ](/docs/auth/login.md)| Обновление jwt токенов |
| POST [/auth/registration ](/docs/auth/login.md)| Регистрация |
| POST [/auth/registration/email-confirm ](/docs/auth/login.md)| Подтверждение email |
| POST [/auth/registration/complete ](/docs/auth/login.md)| Завершение регистрации |
|||
| GET [/users/current ](/docs/auth/login.md)| Получение текущего пользователя |

## Клиентские ошибки:

 - Отправка невалидного JSON вернет в результате **400 Bad Request**

```http
HTTP/1.1 400 Bad Request
```

- Отправка запроса без авторизационного токена вернет в результате **401 Unauthorized**

```http
HTTP/1.1 401 Unauthorized
```
```json
{"message": "Bad Credentials"}
```

- Отправка запроса на несуществующий ресурс вернет в результате **404 Not Found**

```http
HTTP/1.1 404 Not Found
```
```json
{"message": "Not Found"}
```

- Отправка невалидного объекта вернет в результате **422 Unprocessable Entity**

```http
HTTP/1.1 422 Unprocessable Entity
```
```json
{
	"status": 422,
	"errors": {
		"field": "field error"
	},
	"errorMessage": "Failed validation"
}
```