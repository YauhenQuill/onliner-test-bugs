import store from '@/store';

export default async (to, from, next) => {
  if (to.meta.public) {
    return next();
  }

  const isAuth = await store.dispatch('auth/checkAuth');

  if (isAuth) {
    return to.meta.auth ? next() : next({ name: 'main' });
  }

  if (!to.meta.auth) {
    return next();
  }

  return next({ name: 'login' });
};
