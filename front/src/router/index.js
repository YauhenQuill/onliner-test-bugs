import Vue from 'vue'
import Router from 'vue-router'
import EmptyContainer from '@/components/containers/EmptyContainer'
import MainContainer from '@/components/containers/MainContainer'
import Main from '@/components/pages/Main'
import Login from '@/components/pages/Login'
import Registration from '@/components/pages/Registration'

import accessRoute from './middlewares/accessRoute';

Vue.use(Router)

const router = new Router({
  mode: 'history',

  routes: [
    {
      path: '/',
      component: MainContainer,
      children: [
        {
          name: 'main',
          path: '',
          component: Main,
          meta: {
            auth: true,
          },
        },
      ],
    },
    {
      path: '/',
      component: EmptyContainer,
      children: [
        {
          name: 'login',
          path: '/login',
          component: Login,
          meta: {
            auth: false,
          },
        },
        {
          name: 'registration',
          path: '/registration',
          component: Registration,
          meta: {
            auth: false,
          },
        },
      ],
    }
  ]
});

router.beforeEach(accessRoute);

export default router;

