import Vue from 'vue';
import Vuex from 'vuex';

import auth from './modules/auth';
import users from './modules/users';
import tasks from './modules/tasks';
import lists from './modules/lists';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
    auth,
    users,
    tasks,
    lists,
  },
  strict: debug,
});
