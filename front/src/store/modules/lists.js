import listsApiService from '@/services/listsApiService';
import store from "../index";

const state = {
  lists: [],
};

const getters = {};

const actions = {
  async getLists({commit}) {
    try {
      const response = await listsApiService.getLists();

      commit('UPDATE_LISTS', response.data);

      return response;
    } catch (error) {
      throw error;
    }
  },

  async postList({ commit }, payload) {
    try {
      return  await listsApiService.postList(payload);
    } catch (error) {
      throw error;
    }
  },
};

const mutations = {
  UPDATE_LISTS(state, payload) {
    state.lists = payload;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
