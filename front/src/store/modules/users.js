import usersApiService from '@/services/usersApiService';

const state = {};

const getters = {};

const actions = {
  async getUsers(state, payload) {
    try {
      return await usersApiService.getUsers(payload);
    } catch (error) {
      throw error;
    }
  },

  async registrationApprove(state, payload) {
    try {
      return await usersApiService.registrationApprove(payload);
    } catch (error) {
      throw error;
    }
  },

  async registrationDecline(state, payload) {
    try {
      return await usersApiService.registrationDecline(payload);
    } catch (error) {
      throw error;
    }
  },
};

const mutations = {
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
