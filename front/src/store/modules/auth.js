import webStore from 'store2';
import _ from 'underscore';
import moment from 'moment';

import authApiService from '@/services/authApiService';
import usersApiService from '@/services/usersApiService';

import store from '../index';

const state = {
  tokenData: webStore('tokenData') || {},
  currentUser: {},
};

const getters = {
  getTokenData: () => webStore('tokenData'),
  getCurrentUser: () => state.currentUser,
};

const actions = {
  async login({ commit }, payload) {
    try {
      const response = await authApiService.login(payload);

      commit('UPDATE_TOKEN_DATA', response.data);
      await store.dispatch('auth/updateCurrentUser');

      return response;
    } catch (error) {
      commit('UPDATE_TOKEN_DATA');

      throw error;
    }
  },

  async logout({ commit }) {
    commit('UPDATE_TOKEN_DATA');
    commit('UPDATE_CURRENT_USER');
  },

  async registration({ commit }, payload) {
    try {
      const response = await authApiService.registration(payload);

      commit('UPDATE_TOKEN_DATA', response.data);
      await store.dispatch('auth/updateCurrentUser');

      return response;
    } catch (error) {
      throw error;
    }
  },

  async checkAuth({ commit }) {
    if (_.isEmpty(state.currentUser) && !_.isEmpty(state.tokenData)) {
      try {
        await store.dispatch('auth/updateCurrentUser');

        return true;
      } catch (error) {
        commit('UPDATE_TOKEN_DATA');

        return false;
      }
    }

    return !_.isEmpty(state.tokenData);
  },

  async updateCurrentUser({ commit }) {
    try {
      const response = await usersApiService.getCurrentUser();

      commit('UPDATE_CURRENT_USER', response.data);

      return response;
    } catch (error) {
      throw error;
    }
  },
};

const mutations = {
  UPDATE_TOKEN_DATA(state, payload) {
    if (payload) {
      const tokenData = payload;

      tokenData.receiptTime = moment();

      webStore('tokenData', tokenData);
    } else {
      webStore.remove('tokenData');
    }

    state.tokenData = payload;
  },

  UPDATE_CURRENT_USER(state, payload = {}) {
    state.currentUser = payload;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
