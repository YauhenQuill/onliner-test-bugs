import tasksApiService from '@/services/tasksApiService';
import store from "../index";

const state = {
  tasks: {},
};

const getters = {};

const actions = {
  async getTasks({commit}) {
    try {
      const response = await tasksApiService.getTasks();

      commit('UPDATE_TASKS', response.data);

      return response;
    } catch (error) {
      throw error;
    }
  },

  async postTask({ commit }, payload) {
    try {
      const response = await tasksApiService.postTask(payload);

      await store.dispatch('tasks/getTasks');

      return response;
    } catch (error) {
      throw error;
    }
  },

  async complete({ commit }, payload) {
    try {
      const response = await tasksApiService.complete(payload);

      await store.dispatch('tasks/getTasks');

      return response;
    } catch (error) {
      throw error;
    }
  },

  async deleteTask({ commit }, payload) {
    try {
      const response = await tasksApiService.deleteTask(payload);

      await store.dispatch('tasks/getTasks');

      return response;
    } catch (error) {
      throw error;
    }
  },
};

const mutations = {
  UPDATE_TASKS(state, payload) {
    state.tasks = payload;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
