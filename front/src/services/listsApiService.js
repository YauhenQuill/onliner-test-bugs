import api from './api';

const getLists = (params) => {
  const config = {
    url: '/lists',
    method: 'get',
    params,
  };

  return api(config);
};

const postList = (data) => {
  const config = {
    url: '/lists',
    method: 'post',
    data,
  };

  return api(config);
};

export default {
  getLists,
  postList,
};
