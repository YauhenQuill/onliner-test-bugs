import axios from 'axios';

import authInterceptor from './interceptors/auth';
import unauthorized from './interceptors/unauthorized';

const apiInstance = axios.create({
  baseURL: process.env.API_URL,
});

unauthorized(apiInstance);
authInterceptor(apiInstance);

export default apiInstance;
