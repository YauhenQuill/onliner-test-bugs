import app from '@/main';

const interceptor = (apiInstance) => {
  apiInstance.interceptors.response.use(
    response => response,
    (error) => {
      if (error.response.status === 401) {
        app.$store.dispatch('auth/logout');
        app.$router.push({ name: 'login' });
      }

      throw error;
  });
};

export default interceptor;
