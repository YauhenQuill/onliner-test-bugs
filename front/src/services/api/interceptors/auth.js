import webStore from 'store2';

const interceptor = (apiInstance) => {
  apiInstance.interceptors.request.use((config) => {
    const tokenData = webStore('tokenData') || {};

    if (tokenData.accessToken) {
      config.headers.authorization = tokenData.accessToken;
    }

    return config;
  });
};

export default interceptor;
