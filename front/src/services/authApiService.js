import api from './api';

const login = (data) => {
  const config = {
    url: '/auth/login',
    method: 'post',
    data,
  };

  return api(config);
};

const registration = (data) => {
  const config = {
    url: '/auth/registration',
    method: 'post',
    data,
  };

  return api(config);
};

export default {
  login,
  registration,
};
