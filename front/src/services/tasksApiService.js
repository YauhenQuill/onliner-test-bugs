import api from './api';

const getTasks = (params) => {
  const config = {
    url: '/tasks',
    method: 'get',
    params,
  };

  return api(config);
};

const postTask = (data) => {
  const config = {
    url: '/tasks',
    method: 'post',
    data,
  };

  return api(config);
};

const complete = ({id}) => {
  const config = {
    url: `/tasks/${id}/complete`,
    method: 'post',
  };

  return api(config);
};

const deleteTask = ({id}) => {
  const config = {
    url: `/tasks/${id}/delete`,
    method: 'delete',
  };

  return api(config);
};

export default {
  getTasks,
  postTask,
  complete,
  deleteTask,
};
