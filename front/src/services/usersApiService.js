import api from './api';

const getUsers = (params) => {
  const config = {
    url: '/users',
    method: 'get',
    params,
  };

  return api(config);
};

const getCurrentUser = () => {
  const config = {
    url: '/users/current',
    method: 'get',
  };

  return api(config);
};

const registrationApprove = (userId) => {
  const config = {
    url: `/users/${userId}/registration-approve`,
    method: 'post',
  };

  return api(config);
};

const registrationDecline = (userId) => {
  const config = {
    url: `/users/${userId}/registration-decline`,
    method: 'post',
  };

  return api(config);
};

export default {
  getUsers,
  getCurrentUser,
  registrationApprove,
  registrationDecline,
};
